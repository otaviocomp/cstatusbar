#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <X11/Xlib.h>

#include "imodules.h"
#include "util.h"
#include "config.h"

int
main(void)
{
	static Display *dpy;
	char bar[300];
	unsigned long i;

	/* connect to X11 and print values */
	dpy = XOpenDisplay(NULL);
	if (!dpy) {
		fputs("XOpenDisplay: Cannot open display\n", stderr);
		exit(EXIT_FAILURE);
	}

	/* main loop */
	while (1) {
		/* append modules */
		for (i = 0; i < LEN; i++) {
			if (i == 0) {
				strcpy(bar, "");
				func_ptr[i](bar);
			} else
				func_ptr[i](bar);
			if (i != LEN - 1)
				strcat(bar, BIG_SEPARATOR);
		}

		/* display statusbar */
		if (XStoreName(dpy, DefaultRootWindow(dpy), bar) < 0) {
			fputs("XStoreName: allocation failed\n", stderr);
			exit(EXIT_FAILURE);
		}
		XFlush(dpy);
		sleep(1);
	}

	/* close x connection */
	if (XCloseDisplay(dpy) < 0) {
		fputs("xclosedisplay: failed to close display\n", stderr);
		exit(EXIT_FAILURE);
	}

	return EXIT_SUCCESS;
}
