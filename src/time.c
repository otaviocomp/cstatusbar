#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "util.h"

static char *wday(int);

void
time_info(char *bar)
{
	char time_date[29];
	time_t rawtime;
	struct tm *info;

	time(&rawtime);
	info = localtime(&rawtime);

	sprintf(time_date, WHITE "%d %s %02d:%02d" NOCOLOR, info->tm_mday,
	        wday(info->tm_wday), info->tm_hour, info->tm_min);

	strcat(bar, time_date);
}

static char *
wday(int weekday)
{
	switch (weekday) {
		case 0:
			return "Sun";
			break;
		case 1:
			return "Mon";
			break;
		case 2:
			return "Tue";
			break;
		case 3:
			return "Wed";
			break;
		case 4:
			return "Thu";
			break;
		case 5:
			return "Fri";
			break;
		case 6:
			return "Sat";
			break;
		default:
			break;
	}
	/* unreacheable */
	return "Err";
}
