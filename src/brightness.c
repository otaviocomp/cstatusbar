#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

static int bri_max(void);
static int bri_bright(void);
static void bri_fmt(char *, int);

void
bri_info(char *bar)
{
	int bright;
	int maxbright;

	maxbright = bri_max();
	bright    = bri_bright();

	bright = (100 * bright) / maxbright;
	bri_fmt(bar, bright);
}

static int
bri_max(void)
{
	int max;

	if (pscanf(MAX_BRIGHTNESS, "%d", &max) != 1)
		return -1;
	return max;
}

static int
bri_bright(void)
{
	int status;
	int bright;

	status = pscanf(BRIGHTNESS, "%d", &bright);
	if (status != 1)
		return -1;
	return bright;
}

static void
bri_fmt(char *bar, int bright)
{
	char sbright[11];

	/* convert integer to string */
	sprintf(sbright, "%3d", bright);

	strcat(bar, BRIGHT "ﯦ ");
	strcat(bar, sbright);
}
