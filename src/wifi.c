#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "util.h"

#define BUFFER_SIZE 128

static int link(void);
static int convert_link(int);
static void wifi_fmt(char *, int);

void
wifi_info(char *bar)
{
	int link_value;

	link_value = link();
	link_value = convert_link(link_value);

	if (link_value < 20) {
		strcat(bar, RED "直 ");
		wifi_fmt(bar, link_value);
	} else if (link_value < 40) {
		strcat(bar, YELLOW_RED "直 ");
		wifi_fmt(bar, link_value);
	} else if (link_value < 60) {
		strcat(bar, YELLOW "直 ");
		wifi_fmt(bar, link_value);
	} else if (link_value < 80) {
		strcat(bar, GREEN_YELLOW "直 ");
		wifi_fmt(bar, link_value);
	} else {
		strcat(bar, GREEN "直 ");
		wifi_fmt(bar, link_value);
	}

	return;
}

static int
link(void)
{
	char *token;
	char buffer[BUFFER_SIZE];
	FILE *fp;

	fp = fopen("/proc/net/wireless", "r");

	fgets(buffer, BUFFER_SIZE, fp);
	fgets(buffer, BUFFER_SIZE, fp);
	fgets(buffer, BUFFER_SIZE, fp);

	token = strtok(buffer, " ");
	token = strtok(NULL, " ");
	token = strtok(NULL, ".");
	token[0] = token[2];
	token[1] = token[3];
	token[2] = '\0';

	fclose(fp);

	return atoi(token);
}

static int
convert_link(int value)
{
	return (value * 10) / 7;
}

static void
wifi_fmt(char *bar, int link_value)
{
	char slink_value[11];

	sprintf(slink_value, "%3d", link_value);

	strcat(bar, slink_value);

	return;
}
