#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

static void bat_fmt(char *, int);
static int bat_capacity(void);
static int bat_status(void);

void
bat_info(char *bar)
{
	int capacity;
	int batstatus;

	capacity  = bat_capacity();
	batstatus = bat_status();

	if (batstatus == 1)
		if (capacity > 80) {
			strcat(bar, GREEN " ");
			bat_fmt(bar, capacity);
		} else if (capacity > 60) {
			strcat(bar, GREEN_YELLOW " ");
			bat_fmt(bar, capacity);
		} else if (capacity > 40) {
			strcat(bar, YELLOW " ");
			bat_fmt(bar, capacity);
		} else if (capacity > 20) {
			strcat(bar, YELLOW_RED " ");
			bat_fmt(bar, capacity);
		} else {
			strcat(bar, RED " ");
			bat_fmt(bar, capacity);
		}
	else
		if (capacity > 80) {
			strcat(bar, GREEN " ");
			bat_fmt(bar, capacity);
		} else if (capacity > 60) {
			strcat(bar, GREEN_YELLOW " ");
			bat_fmt(bar, capacity);
		} else if (capacity > 40) {
			strcat(bar, YELLOW " ");
			bat_fmt(bar, capacity);
		} else if (capacity > 20) {
			strcat(bar, YELLOW_RED " ");
			bat_fmt(bar, capacity);
		} else {
			strcat(bar, RED " ");
			bat_fmt(bar, capacity);
		}
}

static void
bat_fmt(char *bar, int capacity)
{
	char scapacity[11];

	/* convert integer to string */
	snprintf(scapacity, 11, "%u", capacity);

	strcat(bar, scapacity);
}

#if defined(__linux__)

static int
bat_capacity(void)
{
	int ret;

	if (pscanf(BAT0_CAPACITY, "%d", &ret) != 1)
		if (pscanf(BAT1_CAPACITY, "%d", &ret) != 1)
			return -1;
	return ret;
}

static int
bat_status(void)
{
	int  ret;
	char batstatus[12];

	if (pscanf(BAT0_STATUS, "%s", batstatus) != 1)
		if (pscanf(BAT1_STATUS, "%s", batstatus) != 1)
			return -1;
	if (strcmp(batstatus, "Discharging"))
		ret = 1;
	else
		ret = 0;
		
	return ret;
}

#elif defined(__FreeBSD__)

#include <sys/sysctl.h>
static int
bat_capacity(void)
{
	char buf_battery[] = "hw.acpi.battery.life";
	unsigned int capacity;
	size_t len_capacity;

	if (sysctlbyname(buf_battery, &capacity, &len_capacity, NULL, 0) == -1)
		return 0;
	return capacity;
}

static char *
bat_status(void)
{
	/* TODO */
	char *ret = (char *) malloc(4);
	strcpy(ret, "none");
	return ret;
}

#elif defined(__OpenBSD__)

#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <machine/apmvar.h>

static int
bat_capacity(void)
{
	int apm_fd;
	struct apm_power_info info;

	apm_fd = open("/dev/apm", O_RDONLY);

	if (ioctl(apm_fd, APM_IOC_GETPOWER, &info) == -1) {
		return info.battery_life;
	}

	close(apm_fd);

	return info.battery_life;
}

static int
bat_status(void)
{
	int apm_fd;
	struct apm_power_info info;

	apm_fd = open("/dev/apm", O_RDONLY);

	if (ioctl(apm_fd, APM_IOC_GETPOWER, &info) == -1) {
		return info.ac_state;
	}

	close(apm_fd);

	return info.ac_state;
}
#endif
