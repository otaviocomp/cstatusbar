#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "util.h"

static int  snd_microphone(void);
static int  snd_volume(void);
static void snd_fmt(char *, int);

void
snd_info(char *bar)
{
	int vol, mic;

	mic = snd_microphone();
	vol = snd_volume();

	if ( mic > 0 ) {
		strcat(bar, WHITE "  ");
		snd_fmt(bar, mic);
	} else {
		strcat(bar, RED "  ");
		snd_fmt(bar, mic);
	} if ( vol > 0 ) {
		strcat(bar, WHITE " 墳 ");
		snd_fmt(bar, vol);
	} else {
		strcat(bar, RED " 婢 ");
		snd_fmt(bar, vol);
	}
	strcat(bar, "^d^");
}

#if defined(__linux__)

#include <alsa/asoundlib.h>
static int
snd_microphone(void)
{
	long min, max, mic;
	snd_mixer_t *handle;
	snd_mixer_selem_id_t *sid;
	static const char *card = "default";
	static const char *selem_name= "Capture";

	snd_mixer_open(&handle, 0);
	snd_mixer_attach(handle, card);
	snd_mixer_selem_register(handle, NULL, NULL);
	snd_mixer_load(handle);

	snd_mixer_selem_id_alloca(&sid);
	snd_mixer_selem_id_set_index(sid, 0);
	snd_mixer_selem_id_set_name(sid, selem_name);
	snd_mixer_elem_t *elem = snd_mixer_find_selem(handle, sid);

	snd_mixer_selem_get_capture_volume_range(elem, &min, &max);
	snd_mixer_selem_get_capture_volume(elem, 0, &mic);

	mic = 100 * (mic) / max;
	snd_mixer_close(handle);

	return mic;
}

static int
snd_volume(void)
{
	long min, max, vol;
	snd_mixer_t *handle;
	snd_mixer_selem_id_t *sid;
	static const char *card = "default";
	static const char *selem_name= "Master";

	snd_mixer_open(&handle, 0);
	snd_mixer_attach(handle, card);
	snd_mixer_selem_register(handle, NULL, NULL);
	snd_mixer_load(handle);

	snd_mixer_selem_id_alloca(&sid);
	snd_mixer_selem_id_set_index(sid, 0);
	snd_mixer_selem_id_set_name(sid, selem_name);
	snd_mixer_elem_t *elem = snd_mixer_find_selem(handle, sid);

	snd_mixer_selem_get_playback_volume_range(elem, &min, &max);
	snd_mixer_selem_get_playback_volume(elem, 0, &vol);

	vol = 100 * (vol) / max;
	snd_mixer_close(handle);

	return vol;
}
#elif defined(__FreeBSD__)

#include <sys/soundcard.h>
#define LEN(x) (sizeof (x) / sizeof *(x))

static long
snd_volume(void)
{
	char card[12] = "/dev/mixer0";
	size_t i;
	int v, afd, devmask;
	char *vnames[] = SOUND_DEVICE_NAMES;

	if ((afd = open(card, O_RDONLY | O_NONBLOCK)) < 0) {
		return -1;
	}

	if (ioctl(afd, (int)SOUND_MIXER_READ_DEVMASK, &devmask) < 0) {
		close(afd);
		return -1;
	}
	for (i = 0; i < LEN(vnames); i++) {
		if (devmask & (1 << i) && !strcmp("vol", vnames[i])) {
			if (ioctl(afd, MIXER_READ(i), &v) < 0) {
				close(afd);
				return -1;
			}
		}
	}

	close(afd);
	return v & 0xff;
}

static long
snd_microphone(void)
{
	char card[12] = "/dev/mixer0";
	size_t i;
	int v, afd, devmask;
	char *vnames[] = SOUND_DEVICE_NAMES;

	if ((afd = open(card, O_RDONLY | O_NONBLOCK)) < 0) {
		return -1;
	}

	if (ioctl(afd, (int)SOUND_MIXER_READ_DEVMASK, &devmask) < 0) {
		close(afd);
		return -1;
	}
	for (i = 0; i < LEN(vnames); i++) {
		if (devmask & (1 << i) && !strcmp("rec", vnames[i])) {
			if (ioctl(afd, MIXER_READ(i), &v) < 0) {
				close(afd);
				return -1;
			}
		}
	}

	close(afd);
	return v & 0xff;
}
#endif

static void
snd_fmt(char *bar, int info)
{
	char sinfo[11];

	sprintf(sinfo, "%u", info);

	strcat(bar, sinfo);
}
