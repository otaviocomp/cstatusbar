#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

static unsigned int ram_used(void);
static unsigned int ram_total(void);
static long int ram_percentage(const long int, const long int);
static void ram_fmt(char *, const unsigned int, const unsigned int);

void
ram_info(char *bar)
{
	unsigned int used, total, perc, total_piece;

	used  = ram_used();
	total = ram_total();
	perc  = ram_percentage(used, total);

	/* divide total ram memory available in five parts */
	total_piece = total / 5;

	if (used < total_piece ) {
		strcat(bar, GREEN "M: ");
		ram_fmt(bar , used, perc);
	} else if (used < total_piece * 2) {
		strcat(bar, GREEN_YELLOW "M: ");
		ram_fmt(bar , used, perc);
	} else if (used < total_piece * 3) {
		strcat(bar, YELLOW "M: ");
		ram_fmt(bar , used, perc);
	} else if (used < total_piece * 4) {
		strcat(bar, YELLOW_RED "M: ");
		ram_fmt(bar , used, perc);
	} else {
		strcat(bar, RED "M: ");
		ram_fmt(bar , used, perc);
	}
}

static void
ram_fmt(char *bar, const unsigned int used, const unsigned int perc)
{
	char sused[8];
	char sperc[8];

	/* convert integer to string */
	sprintf(sused, "%u", used);
	sprintf(sperc, "%u", perc);

	strcat(bar, sused);
	strcat(bar, " (");
	strcat(bar, sperc);
	strcat(bar, "%)");
	strcat(bar, NOCOLOR);

	return;
}

#if defined(__linux__)

static unsigned int
ram_total(void)
{
	int status;
	unsigned int total;

	status = pscanf("/proc/meminfo",
			"MemTotal: %u kB\n",
			&total);
	if (status != 1)
		return 0;

	/* convert to megabytes */
	total /= 1024;

	return total;
}

static unsigned int
ram_used(void)
{
	int status;
	unsigned int used, total, free, buffers, cached;

	status = pscanf("/proc/meminfo",
			"MemTotal: %u kB\n"
			"MemFree: %u kB\n"
			"MemAvailable: %u kB\n"
			"Buffers: %u kB\n"
			"Cached: %u kB\n",
			&total, &free, &buffers, &buffers, &cached);
	if (status != 5)
		return 0;

	/* calculate used memory */
	used = total - free - buffers - cached;
	/* convert to megabytes */
	used /= 1024;

	return used;
}

#elif defined(__FreeBSD__)
#include <sys/sysctl.h>

static unsigned int
ram_total(void)
{
	char buf_npages[32] = "vm.stats.vm.v_page_count";
	char buf_page_size[32] = "vm.stats.vm.v_page_size";
	unsigned int npages, page_size;
	size_t len_npages, len_page_size;

	len_npages = sizeof(npages);
	len_page_size = sizeof(page_size);
	if (sysctlbyname(buf_npages, &npages, &len_npages, NULL, 0) == -1)
		return 0;
	if (sysctlbyname(buf_page_size, &page_size, &len_page_size, NULL, 0) == -1)
		return 0;

	return npages * page_size / (1024 * 1024);
}

static unsigned int
ram_used(void)
{
	char buf_active[32] = "vm.stats.vm.v_active_count";
	char buf_wire[32] = "vm.stats.vm.v_wire_count";
	char buf_page_size[32] = "vm.stats.vm.v_page_size";
	unsigned int active, wire, page_size;
	size_t len_active, len_wire, len_page_size;

	len_active = sizeof(active);
	len_wire = sizeof(wire);
	len_page_size = sizeof(page_size);
	if (sysctlbyname(buf_active, &active, &len_active, NULL, 0) == -1)
		return 0;
	if (sysctlbyname(buf_wire, &wire, &len_wire, NULL, 0) == -1)
		return 0;
	if (sysctlbyname(buf_page_size, &page_size, &len_page_size, NULL, 0) == -1)
		return 0;
	return (active + wire) * page_size / (1024 * 1024);
}
#endif

static long int
ram_percentage(const long int used, const long int total)
{
	return (100 * used)/total;
}
