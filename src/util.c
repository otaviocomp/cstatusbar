#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

unsigned long int
convert(long int size, const char unit)
{
	switch (unit) {
		case 'K':
			return size / 1024;
			break;
		case 'M':
			return size / (1024 * 1024);
			break;
		case 'G':
			return size / (1024 * 1024 * 1024);
			break;
		default:
			fprintf(stderr, "Cannot convert");
			break;
	}
	return -1;
}

int
pscanf(const char *path, const char *fmt, ...)
{
	int ret;
	FILE *fp;
	va_list ap;

	fp = fopen(path, "r");
	if (!fp) {
		fprintf(stderr, "Cannot open file %s", path);
		return -1;
	}

	va_start(ap, fmt);
	ret = vfscanf(fp, fmt, ap);
	va_end(ap);
	fclose(fp);

	return (ret == EOF) ? -1 : ret;
}

void
die(const char *msg)
{
	fputs(msg, stderr);
	fputc('\n', stderr);
	exit(EXIT_FAILURE);
}
