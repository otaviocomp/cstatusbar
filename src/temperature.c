#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

static void temp_fmt(char *, unsigned int);
static unsigned int temp_cpu(void); 

void
temp_info(char *bar)
{
	int temp;
	
	temp = temp_cpu();

	if (temp < 50) {
		strcat(bar, GREEN "T: ");
		temp_fmt(bar, temp);
	} else if (temp < 60) {
		strcat(bar, GREEN_YELLOW "T: ");
		temp_fmt(bar, temp);
	} else if (temp < 70) {
		strcat(bar, YELLOW "T: ");
		temp_fmt(bar, temp);
	} else if (temp < 80) {
		strcat(bar, YELLOW_RED "T: ");
		temp_fmt(bar, temp);
	} else {
		strcat(bar, RED "T: ");
		temp_fmt(bar, temp);
	}
}

static void
temp_fmt(char *bar, const unsigned int temp)
{
	char stemp[8];

	sprintf(stemp, "%u", temp);

	strcat(bar, stemp);
}

#if defined(__linux__)

static unsigned int
temp_cpu(void)
{
	unsigned int temp;

	if (pscanf(TEMP_CPU_LINUX, "%u", &temp) != 1)
		return 0;
	return temp/1000;
}

#elif defined(__FreeBSD__)

#include <sys/sysctl.h>

static unsigned int
temp_cpu(void)
{
	unsigned int temp;
	size_t len;
	char buf[32] = TEMP_CPU_FREEBSD;

	len = sizeof(temp);
	if (sysctlbyname(buf, &temp, &len, NULL, 0) == -1)
		return 0;

	return (temp - 2731) / 10;
}
#endif
