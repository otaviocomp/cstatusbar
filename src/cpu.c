#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

static int cpu_perc(void);

void
cpu_info(char *bar)
{
	int cpu;

	cpu = cpu_perc();

	if (cpu < 1)
		strcat(bar, CPU_COLOR0);
	else if (cpu < 10)
		strcat(bar, CPU_COLOR1);
	else if (cpu < 20)
		strcat(bar, CPU_COLOR2);
	else if (cpu < 30)
		strcat(bar, CPU_COLOR3);
	else if (cpu < 40)
		strcat(bar, CPU_COLOR4);
	else if (cpu < 50)
		strcat(bar, CPU_COLOR5);
	else if (cpu < 60)
		strcat(bar, CPU_COLOR6);
	else if (cpu < 70)
		strcat(bar, CPU_COLOR7);
	else if (cpu < 80)
		strcat(bar, CPU_COLOR8);
	else if (cpu < 90)
		strcat(bar, CPU_COLOR9);
	else
		strcat(bar, CPU_COLOR10);
}

#if defined(__linux__)
static int
cpu_perc(void)
{
	static unsigned long int cpu[7];
	unsigned long int prevcpu[7], total, idle, perc, status;

	memcpy(prevcpu, cpu, sizeof(prevcpu));
	/* user nice system idle iowait irq softirq */
	status = pscanf("/proc/stat", 
			"cpu %lu %lu %lu %lu %lu %lu %lu",
			&cpu[0], &cpu[1], &cpu[2], &cpu[3], &cpu[4], &cpu[5],
			&cpu[6]);
	if (status != 7)
		return -1;
	idle = (cpu[3] + cpu[4]) - (prevcpu[3] + prevcpu[4]);
	total = (cpu[0] + cpu[1] + cpu[2] + cpu[3] + cpu[4] + cpu[5] + cpu[6]) -
		(prevcpu[0] + prevcpu[1] + prevcpu[2] + prevcpu[3] + prevcpu[4] +
		prevcpu[5] + prevcpu[6]);
	perc = ((total - idle) * 100)/ total;
	return perc;
}
#elif defined(__FreeBSD__)

#include <sys/param.h>
#include <sys/sysctl.h>
#include <devstat.h>

static int
cpu_perc(void)
{
	size_t size;
	static long a[CPUSTATES];
	long b[CPUSTATES], sum;

	size = sizeof(a);
	memcpy(b, a, sizeof(b));
	if (sysctlbyname("kern.cp_time", &a, &size, NULL, 0) == -1
			|| !size) {
		return -1;
	}
	sum = (a[CP_USER] + a[CP_NICE] + a[CP_SYS] + a[CP_INTR] + a[CP_IDLE]) -
	      (b[CP_USER] + b[CP_NICE] + b[CP_SYS] + b[CP_INTR] + b[CP_IDLE]);

	if (sum == 0) {
		return -1;
	}

	return 100 * ((a[CP_USER] + a[CP_NICE] + a[CP_SYS] +
			 a[CP_INTR]) - (b[CP_USER] + b[CP_NICE] + b[CP_SYS] +
			 b[CP_INTR])) / sum;

}
#elif defined(__OpenBSD__)

#include <sys/sched.h>
#include <sys/sysctl.h>

static int
cpu_perc(void)
{
	int total;
	int idle;
	int mib[2];
	size_t size;
	static long cpu[CPUSTATES];
	long oldcpu[CPUSTATES];

	mib[0] = CTL_KERN;
	mib[1] = KERN_CPTIME;
	size = sizeof(cpu);
	memcpy(oldcpu, cpu, sizeof(oldcpu));

	if (sysctl(mib, 2, &cpu, &size, NULL, 0) == -1) {
		return -1;
	}

	total = (cpu[CP_USER] + cpu[CP_NICE] + cpu[CP_SYS] + cpu[CP_IDLE]) - 
	        (oldcpu[CP_USER] + oldcpu[CP_NICE] + oldcpu[CP_SYS] +
		oldcpu[CP_IDLE]);
	if (total == 0) {
		return -1;
	}
	idle = cpu[CP_IDLE] - oldcpu[CP_IDLE];

	return ((total - idle) * 100) / total;
}
#endif
