#include <sys/statvfs.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include "util.h"

static long disk_used(void);
static long disk_total(void);
static unsigned int disk_percentage(const long, const long);
static void disk_fmt(char *, const long, const unsigned int);

void
disk_info(char *bar)
{
	long used, total, perc, total_piece;

	used  = disk_used();
	total = disk_total();
	perc  = disk_percentage(used, total);

	/* divide total disk memory available in five parts */
	total_piece = total / 5;

	if (used < total_piece ) {
		strcat(bar, GREEN " ");
		disk_fmt(bar, used, perc);
	} else if (used < total_piece * 2) {
		strcat(bar, GREEN_YELLOW " ");
		disk_fmt(bar, used, perc);
	} else if (used < total_piece * 3) {
		strcat(bar, YELLOW " ");
		disk_fmt(bar, used, perc);
	} else if (used < total_piece * 4) {
		strcat(bar, YELLOW_RED " ");
		disk_fmt(bar, used, perc);
	} else {
		strcat(bar, RED " ");
		disk_fmt(bar, used, perc);
	}
}

static long
disk_used(void)
{
	struct statvfs fs;

	if (statvfs("/", &fs) < 0)
		return -1;
	return convert(fs.f_frsize * (fs.f_blocks - fs.f_bfree), 'M');
}

static long
disk_total(void)
{
	struct statvfs fs;

	if (statvfs("/", &fs) < 0)
		return -1;
	return convert(fs.f_frsize * fs.f_blocks, 'M');
}

static unsigned int 
disk_percentage(const long used, const long total)
{
	return (100 * used)/total;
}

static void
disk_fmt(char *bar, const long used, const unsigned int perc)
{
	char sused[16];
	char sperc[16];

	/* convert integer to string */
	sprintf(sused, "%lu", used);
	sprintf(sperc, "%u", perc);

	strcat(bar, sused);
	strcat(bar, " (");
	strcat(bar, sperc);
	strcat(bar, "%)");

	return;
}
