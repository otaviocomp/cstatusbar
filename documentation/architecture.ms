.PS
MC: box "cstatusbar.c" wid 1.5 at (0, 1)
M: box "imodules.h" at (1.5, 1)
UH: box "util.h" at (5.5, 1)
U: box "util.c" at (5.5, 2)
CFG: box "config.h" at (0, 2)

###########
# modules #
###########

A: box "cpu.c" wid 1.2 at         (3.5, 5)
B: box "temperature.c" wid 1.2 at (3.5, 4)
C: box "ram_memory.c" wid 1.2 at  (3.5, 3)
D: box "disk.c" wid 1.2 at        (3.5, 2)
E: box "wifi.c" wid 1.2 at        (3.5, 1)
F: box "sound.c" wid 1.2 at       (3.5, 0)
G: box "brightness.c" wid 1.2 at  (3.5, -1)
H: box "time.c" wid 1.2 at        (3.5, -2)

line from (2.5, 5) to (2.5, -2)
line from A.w to (2.5, 5)
line from B.w to (2.5, 4)
line from C.w to (2.5, 3)
line from D.w to (2.5, 2)
line from E.w to (2.5, 1)
line from F.w to (2.5, 0)
line from G.w to (2.5, -1)
line from H.w to (2.5, -2)

line from  (4.5, 5)  to (4.5, -2)
arrow from (4.5, 5)  to A.e 
arrow from (4.5, 4)  to B.e 
arrow from (4.5, 3)  to C.e 
arrow from (4.5, 2)  to D.e 
arrow from (4.5, 1)  to E.e 
arrow from (4.5, 0)  to F.e
arrow from (4.5, -1) to G.e 
arrow from (4.5, -2) to H.e 

arrow from CFG.s to MC.n
arrow from M.w to MC.e
arrow from (2.5, 1) to M.e
line from UH.w to (4.5, 1)
arrow from U.s to UH.n
line from UH.s to (5.5, -3)
line from (5.5, -3) to (0, -3)
arrow from (0, -3) to MC.s

.PE
