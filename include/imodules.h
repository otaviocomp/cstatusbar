#ifndef STATUSBAR_H
#define STATUSBAR_H

void time_info(char *);

void bat_info(char *);

void bri_info(char *);

void snd_info(char *);

void ram_info(char *);

void disk_info(char *);

void temp_info(char *);

void cpu_info(char *);

void wifi_info(char *);

#endif
