#ifndef UTIL_H
#define UTIL_H

/* paths */
#define TEMP_CPU_LINUX   "/sys/class/thermal/thermal_zone0/temp"
#define TEMP_CPU_FREEBSD "hw.acpi.thermal.tz0.temperature"
#define MAX_BRIGHTNESS   "/sys/class/backlight/acpi_video0/max_brightness"
#define BRIGHTNESS       "/sys/class/backlight/acpi_video0/brightness"
#define BAT0_CAPACITY    "/sys/class/power_supply/BAT0/capacity"
#define BAT0_STATUS      "/sys/class/power_supply/BAT0/status"
#define BAT1_CAPACITY    "/sys/class/power_supply/BAT1/capacity"
#define BAT1_STATUS      "/sys/class/power_supply/BAT1/status"
/* separator bar */
#define SMALL_SEPARATOR "  ^c#FF0000#^^r0,0,1,19^^f1^^d^  "
#define BIG_SEPARATOR   "  ^c#FFFF00#^^r0,0,2,19^^f2^^d^  "
/* Colors */
#define GREEN        "^c#00FF00^"
#define GREEN_YELLOW "^c#9FFF00^"
#define YELLOW       "^c#FFFF00^"
#define YELLOW_RED   "^c#FF8000^"
#define RED          "^c#FF0000^"
#define WHITE        "^c#FFFFFF^"
#define BRIGHT       "^c#FBF547^"
#define NOCOLOR      "^d^"
/* CPU colors */
#define CPU_COLOR0  "^c#226622^CPU: ^r0,0,7,19^^f7^"
#define CPU_COLOR1  "^c#04FF00^CPU: ^c#226622^^r0,0,7,19^^f7^^c#04FF00^^r-7,17,7,2^^d^"
#define CPU_COLOR2  "^c#0BFF00^CPU: ^c#226622^^r0,0,7,19^^f7^^c#0BFF00^^r-7,15,7,4^^d^"
#define CPU_COLOR3  "^c#BBBB00^CPU: ^c#666622^^r0,0,7,19^^f7^^c#BBBB00^^r-7,13,7,6^^d^"
#define CPU_COLOR4  "^c#BBBB00^CPU: ^c#666622^^r0,0,7,19^^f7^^c#BBBB00^^r-7,11,7,8^^d^"
#define CPU_COLOR5  "^c#FFFF00^CPU: ^c#886622^^r0,0,7,19^^f7^^c#FFFF00^^r-7,9,7,10^^d^"
#define CPU_COLOR6  "^c#FFFF00^CPU: ^c#886622^^r0,0,7,19^^f7^^c#FFFF00^^r-7,7,7,12^^d^"
#define CPU_COLOR7  "^c#FFBB00^CPU: ^c#AA6622^^r0,0,7,19^^f7^^c#FFBB00^^r-7,5,7,14^^d^"
#define CPU_COLOR8  "^c#FFBB00^CPU: ^c#AA2222^^r0,0,7,19^^f7^^c#FFBB00^^r-7,3,7,16^^d^"
#define CPU_COLOR9  "^c#FF0000^CPU: ^c#BB2222^^r0,0,7,19^^f7^^c#FF0000^^r-7,1,7,18^^d^"
#define CPU_COLOR10 "^c#FF0000^CPU: ^c#BB2200^^r0,0,7,19^^f7^^c#FF0000^^r-7,0,7,20^^d^"

/* convert units */
unsigned long int convert(long int size, const char unit);
/* path scanf */
int pscanf(const char *path, const char *fmt, ...);
/* die function */
void die(const char *msg);

/* refresh */
/*unsigned int refresh(unsigned int set);*/

#endif
