#define LEN sizeof(func_ptr)/sizeof(func_ptr[0])

/* ALL AVAILABLE MODULES
 *
 * cpu_info  - instantaneous CPU load
 * temp_info - temperature of CPU
 * ram_info  - ram memory in megabytes and percentage (used/total)
 * disk_info - disk usage in megabytes and percentage (used/total)
 * wifi_info - wireless strengh signal
 * snd_info  - microphone and volume values
 * bri_info  - brightness of screen
 * bat_info  - battery level
 * time_info - date and time 
 *
 */

void (*func_ptr[])(char *) = {
	/* comment or uncomment for disable or enable modules, respectively */
	/* don't forget to recompile :) */
	cpu_info,
	temp_info,
	ram_info,
	disk_info,
	wifi_info,
	snd_info,
	bri_info,
	bat_info,
	time_info
};
