# cstatusbar

cstatusbar, a colorful, fast, lightweight, and simple statusbar for
the dwm tiling window manager.

## Dependencies
- dwm tiling window manager
- Requires the status2D patch to work.

## Usage

The modules can be selected editing the "config.h" file, just uncomment the
name of the function to enable or comment to disable.

## Images

![Alt text](images/cstatusbar.png?raw=true "cstatusbar")
