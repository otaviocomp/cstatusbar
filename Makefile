# VERSION
VERSION = 0.3

# Binary name
BIN     = cstatusbar

# PATHS
DESTBIN = /usr/local/bin
DESTMAN = /usr/local/share/man/man1
INCDIR  = -Iinclude -I.
# Uncomment for Openbsd
#X11INC = -I/usr/X11R6/include
#X11LIB = -L/usr/X11R6/lib

# FLAGS
CFLAGS  = -Wall -Wextra -Werror -O3 -march=native

LDLIBS  = -lX11
# for linux systems
LDLIBS  = -lX11 -lasound

# Modules
MOD     = src/battery\
          src/brightness\
          src/cpu\
          src/cstatusbar\
          src/disk\
          src/ram_memory\
          src/sound\
          src/temperature\
          src/time\
          src/util\
          src/wifi

all: $(BIN)

$(MOD:=.o): config.h

# Rules
.c.o:
	cc $(CFLAGS) $(INCDIR) $(X11INC) -c $< -o $@

$(BIN): $(MOD:=.o)
	cc $(LDLIBS) $(X11LIB) $(MOD:=.o) -o $@

install: $(BIN)
	mkdir -p $(DESTBIN)
	cp -f $(BIN) $(DESTBIN)
	chmod 755 $(DESTBIN)/$(BIN)
	mkdir -p $(DESTMAN)
	sed "s/VERSION/$(VERSION)/g" < documentation/cstatusbar.1 > $(DESTMAN)/cstatusbar.1
	chmod 644 $(DESTMAN)/cstatusbar.1

uninstall:
	rm -f $(DESTBIN)/$(BIN)
	rm -f $(DESTMAN)/cstatusbar.1

clean:
	rm -f $(BIN) $(MOD:=.o)

.PHONY: all install uninstall clean 
